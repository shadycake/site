var index = 0;
var alphabet = "abdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!£$%^&*()_+[]{};'#:@~,./<>?|\\\"";

var elements = document.getElementsByClassName("cipher");
var max_step = 5;
var progress = 1000000;

// Filter out elements with the cipher effect
var cursors = [];
var fullCursors = document.getElementsByClassName("cursor");
cursors = Array.prototype.slice.call(fullCursors);

// Cursor blinking
setInterval(function() {
    for (var i=0; i<cursors.length; i++) {
        cursor = cursors[i];
        if (cursor.innerHTML.endsWith("_")) {
            cursor.innerHTML = cursor.innerHTML.substring(0, cursor.innerHTML.length - 1);
        } else {
            cursor.innerHTML += "_";
        }
    }
}, 500);

function cipher(element) {
    if (element.classList.contains("cursor")) {
        cursors.splice(cursors.indexOf(element), 1);
    }
    var step = 0;
    var char = -1;
    var plaintext = element.innerHTML;
    var instance = setInterval(function(){
        var ciphertext = "";
        for (j=0; j<Math.min(char+progress, plaintext.length); j++) {
            if (plaintext[j] == " ") {
                ciphertext += " ";
            }
            else if (j <= char) {
                ciphertext += plaintext[j];
            } else {
                var character = alphabet[Math.floor(Math.random()*alphabet.length)];
                ciphertext += "&#" + character.charCodeAt() + ";";
            }
        }
        element.innerHTML = ciphertext;
        step++;
        if (step > max_step) {
            char++;
            step = 0;
        }
        if (char > plaintext.length) {
            if (element.classList.contains("cursor")) {
                cursors.push(element);
            }
            clearInterval(instance);
        }
    }, 30)
}

for (var i=0; i<elements.length; i++) {
    cipher(elements[i])
}


// Dropdown menu
var dropdownButton = document.getElementById("nav-dropdown-button");
var dropdownMenu = document.getElementById("nav-dropdown-menu");

console.log(dropdownMenu.style.display);
dropdownButton.onclick = function(){
    if (dropdownMenu.style.display == "") {
        dropdownMenu.style.display = "block";
        dropdownButton.innerHTML = "/\\";
    } else {
        dropdownMenu.style.display = ""
        dropdownButton.innerHTML="\\/";
    }
}